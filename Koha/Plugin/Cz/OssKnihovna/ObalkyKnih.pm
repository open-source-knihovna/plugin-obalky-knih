package Koha::Plugin::Cz::OssKnihovna::ObalkyKnih;

# Copyright 2019-2021 Josef Moravec, Radek Šiman
#
# This file is part of koha-plugin-obalkyknih
#
# koha-plugin-obalkyknih is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# koha-plugin-obalkyknih is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details. #
#
# You should have received a copy of the GNU General Public License
# along with koha-plugin-obalkyknih; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;
use utf8;

use base qw(Koha::Plugins::Base);
use C4::Context;

use Koha::Plugin::Cz::OssKnihovna::ObalkyKnih::Service;

# Plugin version. Filled by the build tools.
our $VERSION = "{VERSION}";
$VERSION = '1.0.3';

our $metadata = {
    name            => 'ObalkyKnih.cz',
    author          => 'Josef Moravec',
    description     => 'Covers and more from Obalkyknih.cz service',
    date_authored   => '2019-11-30',
    date_updated    => '2024-10-18',
    minimum_version => '21.11',
    maximum_version => undef,
    version         => $VERSION,
};

=head1 Plugin methods

=head2 new

A constructor

=cut

sub new {
    my ($class, $args) = @_;

    $args->{'metadata'} = $metadata;

    my $self = $class->SUPER::new($args);
    $self->{'service'} = Koha::Plugin::Cz::OssKnihovna::ObalkyKnih::Service->new({
        sigla => $self->retrieve_data('sigla')
    });

    return $self;
}

=head2 install

Installs the plugin

=cut

sub install {
    return 1;
}

sub configure {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('save') ) {
        my $template = $self->get_template({ file => 'configure.tt' });

        ## Grab the values we already have for our settings, if any exist

        $template->param(
            sigla => $self->retrieve_data('sigla'),
        );

        print $cgi->header(-type => 'text/html',
                           -charset => 'utf-8');
        print $template->output();
    }
    else {
        $self->store_data(
            {
                sigla => scalar $cgi->param('sigla'),
                last_configured_by => C4::Context->userenv->{'number'},
            }
        );

        $self->go_home();
    }
}

=head2 get_cover

Get cover hoook

=cut

sub cover_html {
    my ( $self, $biblio ) = @_;

    my @metadata = $self->{service}->get_book_metadata( $biblio );
    my $itemdata = $metadata[0];

    return unless $itemdata->{cover_medium_url};

    my $img_url = $itemdata->{cover_medium_url};
    my $backlink_url = $itemdata->{backlink_url};
    my $full_size_url = $itemdata->{cover_preview510_url};

    return "<div class='cover-obalkyknih'>
            <a href='$full_size_url' class='cover-obalkyknih'><img src='$img_url'/></a>
            <br><a target='_blank' href='$backlink_url' class='backlink'>Obálka ze serveru ObálkyKnih.cz</a>
            </div>";
}

sub intranet_js {
    return q|<script>
        var link=$('div.cover-obalkyknih a.backlink');
        $('div.cover-obalkyknih').parent().parent().append(link);
	</script>|;
}

sub opac_cover_html {
    my ( $self, $biblio ) = @_;

    return $self->cover_html( $biblio );
}

sub intranet_cover_html {
    my ( $self, $biblio ) = @_;

    return $self->cover_html( $biblio );
}

sub cover_thumbnails_html {
    my ( $self, $results ) = @_;

    return unless $results;

    my @biblios;
    foreach my $result ( @$results ) {
        my $biblio = $result->{biblio_object};
        $biblio = Koha::Biblios->find($result->{biblionumber}) unless $biblio;
        push @biblios, $biblio;
    }
    my @metadata = $self->{service}->get_book_metadata_batch( \@biblios );

    my @thumbnails;
    foreach my $data ( @metadata ) {
        push @thumbnails, "<span class='no-image'>Obálka nebyla nalezena</span>" unless $data->{cover_medium_url};

        if ( $data->{cover_medium_url} ) {
            my $img_url = $data->{cover_medium_url};
            push @thumbnails, "<img src='$img_url'>";
        }
    }
    return \@thumbnails;
}

sub toc_html {
    my ( $self, $biblio ) = @_;

    my @metadata = $self->{service}->get_book_metadata( $biblio );
    my $itemdata = $metadata[0];

    return unless $itemdata->{toc_pdf_url};

    my $thumbnail_url = $itemdata->{toc_thumbnail_url};
    my $toc_pdf_url = $itemdata->{toc_pdf_url};
    my $backlink_url = $itemdata->{backlink_url};

    return "<div class='toc-obalkyknih'><a target='_blank' href='$toc_pdf_url'><img src='$thumbnail_url'/></a><br><a target='_blank' href='$backlink_url'>Obsah ze serveru ObálkyKnih.cz</a></div>";
}

sub intranet_catalog_biblio_tab {
    my ( $self, $args ) = @_;
    my @tabs;

    require Koha::Plugins::Tab;

    my $toc_html = $self->toc_html( $args->{biblio} );

    push @tabs, Koha::Plugins::Tab->new({
            title => 'Obsah',
            content => $toc_html
        }) if $toc_html;

    return @tabs;
}

sub get_rating {
    my ( $self, $biblio ) = @_;

    my @metadata = $self->{service}->get_book_metadata( $biblio );
    my $itemdata = $metadata[0];

    return 0;
}

sub get_comments {
    my ( $self, $biblio ) = @_;

    my @metadata = $self->{service}->get_book_metadata( $biblio );
    my $itemdata = $metadata[0];

    return 0;
}

sub get_annotation {
    my ( $self, $biblio ) = @_;

    my @metadata = $self->{service}->get_book_metadata( $biblio );
    my $itemdata = $metadata[0];

    return 0;
}

sub author_cover_html {
    my ( $self, $authority ) = @_;

    my @metadata = $self->{service}->get_authority_metadata( $authority );
    my $itemdata = $metadata[0];

    return unless $itemdata->{cover_medium_url};

    my $img_url = $itemdata->{cover_medium_url};
    my $backlink_url = $itemdata->{backlink_url};
    my $full_size_url = $itemdata->{cover_preview510_url};

    return "<div class='cover-auth-obalkyknih'>
        <a href='$full_size_url' data-toggle='modal' data-target='#obalkyAuthKnihModal'><img src='$img_url'/></a>
        <br><a target='_blank' href='$backlink_url'>Foto ze serveru ObálkyKnih.cz</a></div>
        <div id='obalkyAuthKnihModal' class='modal fade' tabindex='-1' role='dialog'>
        <div class='modal-dialog modal-lg'><div class='modal-content'><img src='$full_size_url'></div></div></div>";

    return 0;
}

sub get_citation {
    my ( $self, $biblio ) = @_;

    my @metadata = $self->{service}->get_book_metadata( $biblio );
    my $itemdata = $metadata[0];
    my $citation = $itemdata->{csn_iso_690};
    return $citation;
}


sub intranet_catalog_biblio_enhancements_toolbar_button {
    my ( $self, $biblio ) = @_;
    my $citation = $self->get_citation( $biblio );
    return unless $citation;
    return "<a id='obalkyknih-citation' class='btn btn-default' data-toggle='modal' data-target='#obalkyKnihCitationModal'>Zobrazit citaci</a>
        <div id='obalkyKnihCitationModal' class='modal fade' tabindex='-1' role='dialog'>
        <div class='modal-dialog' role='document'><div class='modal-content'>
        <div class='modal-header'>
            <button type='button' class='closebtn' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            <h4 class='modal-title'>Citace podle ČSN ISO 690</h4></div>
        <div class='modal-body'>$citation</div></div></div></div>";
}

return 1;
