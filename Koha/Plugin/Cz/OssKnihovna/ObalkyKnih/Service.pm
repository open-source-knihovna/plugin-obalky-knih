package Koha::Plugin::Cz::OssKnihovna::ObalkyKnih::Service;

# Copyright 2019 Josef Moravec
#
# This file is part of koha-plugin-obalkyknih
#
# koha-plugin-obalkyknih is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# koha-plugin-obalkyknih is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details. #
#
# You should have received a copy of the GNU General Public License
# along with koha-plugin-obalkyknih; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;

use JSON;
use LWP::UserAgent ();
use URI::Escape;

use C4::AuthoritiesMarc qw( GetAuthority );
use C4::Context;
use Koha;
use Koha::Caches;

=head1 Plugin constants

=head2 $api_endpoints

=cut

our $api_endpoints = {
    'BOOKS'     => '/api/books',
    'COVER'     => '/api/cover',
    'TOC'       => '/api/toc',
    'AUTHORITY' => '/api/auth/meta',
    'CITATION'  => ':8080/api/citace',
    'RECOMMEND' => '/api/doporuc',
    'ALIVE'     => '/api/runtime/alive'
};

=head2 @api_bases

=cut

our @api_bases = (
    'https://cache.obalkyknih.cz',
    'https://cache2.obalkyknih.cz',
    'https://cache3.obalkyknih.cz'
);

=head1 Plugin methods

=head2 new

A constructor

=cut

sub new {
    my ( $class, $args ) = @_;

    $args ||= {};
    return bless( $args, $class );
}

sub _http {
    my ( $self ) = shift;

    unless ( $self->{_http_client} ) {
        $self->{_http_client} = LWP::UserAgent->new(
            agent => "Koha " . $Koha::VERSION,
            timeout => 2,
            protocols_allowed => [ 'http', 'https' ]
        );
        $self->{_http_client}->default_header( 'Referer' => C4::Context->preference('OPACBaseURL') );
    }

    return $self->{_http_client};
}

sub _cache {
    my ( $self ) = shift;
    $self->{_cache_instance} ||= Koha::Caches->get_instance( 'plugin-obalkyknih' );
    return $self->{_cache_instance};
}

sub _get_alive_base_url {
    my ( $self ) = shift;

    my $cached_url = $self->_cache->get_from_cache( 'base-url' );
    return $cached_url if $cached_url;

    foreach my $url ( @api_bases ) {
        my $response = $self->_http->get( $url . $api_endpoints->{'ALIVE'} );
        if ($response->is_success) {
            $self->_cache->set_in_cache( 'base-url', $url, { expiry => 60} );
            return $url;
        }
    }

    return $api_bases[0];
}

sub _get_book_metadata_from_service {
    my ( $self, $biblio ) = @_;
    my $http = $self->_http;
    my $url =  $self->_get_alive_base_url . $api_endpoints->{'BOOKS'};

    my @params = $self->_extract_book_params( $biblio );
    my $resource = $http->get(
        $url . '?multi=' . encode_json( \@params )
    );

    return {} unless $resource->is_success;

    my @data =  @{decode_json( $resource->decoded_content )};

    $self->_cache->set_in_cache( $biblio->biblionumber, \@data, { expiry => 600} );
    return @data;
}

sub _extract_book_params {
    my ( $self, $biblio ) = @_;
    my @params = ();
    my $param = {};
    my $biblioitem = $biblio->biblioitem;
    my $record = $biblio->metadata->record;

    $param->{isbn} = $biblioitem->isbn || $biblioitem->ean || $biblioitem->issn
        if ( $biblioitem->isbn || $biblioitem->ean || $biblioitem->issn );

    #TODO This does work only for ISMN-10, ISMN-13 needs to be added to isbn param
    my $ismn_field = $record->field( '024' );
    if ( $ismn_field ) {
    	
        # The obalkyknih.cz service expects ISMN in ISBN field, if ISBN is empty
    	if ( exists $param->{isbn} ) {
    		$param->{ismn} = $ismn_field->subfield( 'a' )
    	}
    	else {
    		$param->{isbn} = $ismn_field->subfield( 'a' )
    	}
    }

    my $nbn_field = $record->field( '015' );
    $param->{nbn} = $nbn_field->subfield( 'a' ) if $nbn_field;
    $param->{nbn} = $self->{sigla} . '-' . $biblio->biblionumber unless $nbn_field && $self->{sigla};

    my $oclc_field = $record->field( '035' );
    $param->{oclc} = $oclc_field->subfield( 'a' ) if $oclc_field;

    push @params, $param;
    return @params;
}

=head2 get_book_metadata

    my @metadata = $service->get_book_metadata( $biblio );

    returns array of hashes of metadata from ObalkyKnih API,
    $biblio is instance of Koha::Biblio

=cut

sub get_book_metadata {
    my ( $self, $biblio ) = @_;

    my $metadata = $self->_cache->get_from_cache( $biblio->biblionumber );

    return @$metadata if $metadata;
    return $self->_get_book_metadata_from_service( $biblio );
}

=head2 get_book_metadata_batch

    my @metadata = $service->get_book_metadata_batch( $biblios );

    returns array of hashes of metadata from ObalkyKnih API,
    $biblios is arrayref of instances of Koha::Biblio

=cut

sub get_book_metadata_batch {
    my ( $self, $biblios ) = @_;

    my @result;
    foreach my $biblio ( @$biblios ) {
        my $metadata = $self->_cache->get_from_cache( $biblio->biblionumber );
        push @result, @$metadata if $metadata;
        push @result, $self->_get_book_metadata_from_service( $biblio ) unless $metadata;
    }

    return @result;
}

sub _get_authority_metadata_from_service {
    my ( $self, $authority ) = @_;
    my $http = $self->_http;
    my $url =  $self->_get_alive_base_url . $api_endpoints->{'AUTHORITY'};

    my $record = GetAuthority( $authority->authid );
    my $heading_field = $record->field( '100' );
    return unless $heading_field;

    my $nkp_id = $heading_field->subfield( '7' );
    return unless $nkp_id;

    my $resource = $http->get(
        $url . '?auth_id=' . uri_escape($nkp_id)
    );

    return unless $resource->is_success;

    my @data =  @{decode_json( $resource->decoded_content )};

    $self->_cache->set_in_cache( 'auth-' .  $authority->authid, \@data, { expiry => 600} );
    return @data;
}

=head2 get_authority_metadata

    my @metadata = $service->get_authority_metadata( $authority )

    returns array of hashes of authority metadta from ObalkyKnih API
    $authority is instance of Koha::Authority

=cut

sub get_authority_metadata {
    my ( $self, $authority ) = @_;

    my $metadata = $self->_cache->get_from_cache( 'auth-' . $authority->authid );

    return @$metadata if $metadata;
    return $self->_get_authority_metadata_from_service( $authority );
}

return 1;
